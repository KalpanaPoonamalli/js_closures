function cacheFunction(cb){
    let cacheObject = {};
    if (typeof cb !== 'function'){
        throw new error("Please provide valid parameters")
    }

    return function(...args){

        let array = JSON.stringify(args)

        if(cacheObject[array]){
            return cacheObject[array];
        }else{
            let result = cb(...args);
            cacheObject[array] = result
            return result
        }
    }
}

module.exports = cacheFunction;