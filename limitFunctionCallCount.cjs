function limitFunctionalCallCount(cb, n){
    let arrayValue = 0;

    if (typeof cb !== 'function' || typeof n !== "number" || n <= 0){
        throw new error("Please provide valid parameters")
    }

    return function(...args){
        if (arrayValue >= n){
            return null;
        }else {
        arrayValue += 1
        return cb(...args)
        }
     }
}

module.exports = limitFunctionalCallCount;