let limitFunctionalCallCount = require('../limitFunctionCallCount.cjs');
    
function cb(number){
   // console.log(number*number);
    return number ;
}

console.log(typeof cb)

const result = limitFunctionalCallCount(cb, 4);

try {
    console.log(result("Kalpana"));
    console.log(result("Venky"));
    console.log(result("Ram"));
    console.log(result());
    console.log(result());
}
catch(error) {
    console.error(error.message)
}